// ТЕОРІЯ
/*
1. 
Цикл - це блок коду, завдання якого здійснити повторення самого себе до визначеного ним моменту зупинки.

2. В JS є наступні такі цикли: 

for;
while;
do / while;

3. На відміну від циклу while, цикл do while завжди виконує оператор хоча б один раз, перед оцінкою виразу, його ще називають циклом перевірки.
*/



// ПРАКТИКА

// 1.

let numOne = parseInt(prompt("Введіть ,будь ласка ,перше число"));
let numTwo = parseInt(prompt("Введіть ,будь ласка ,друге число"));

while (numOne !== Number(numOne)){
    numOne = parseInt(prompt("Введіть ,будь ласка ,перше число"));
}
while (numTwo !== Number(numTwo)){
    numTwo = parseInt(prompt("Введіть ,будь ласка ,друге число"));
}

if(numOne <= numTwo){
    for(let i = numOne; i <= numTwo; i++){
        console.log(i);
    }
}else{
    for(let i = numTwo; i <= numOne; i++){
        console.log(i);
    }
}

// 2.

let even = parseInt(prompt("Введіть парне число"));

while(even % 2 !== 0){
    even = parseInt(prompt("Введіть парне число"));
}
console.log(even);
